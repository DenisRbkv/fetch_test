import 'dart:convert';
import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:fetch_test/repo/dto_post.dart';
import 'package:fetch_test/validating_func.dart';

Future<Album> createAlbum(String title) async {
  final http.Response response = await http.post(
    //'https://jsonplaceholder.typicode.com/albums'
    url,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
      'title': title,
    }),
  );

  if (response.statusCode == 201) {
    return Album.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to create album.');
  }
}
