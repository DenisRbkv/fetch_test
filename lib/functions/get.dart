import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;

import 'package:fetch_test/repo/dto_get.dart';
import 'package:fetch_test/validating_func.dart';


Future<AlbumGet> fetchAlbum() async {
  final response =
  await http.get(url);
      //'http://numbersapi.com/random/trivia?json'


  if (response.statusCode == 200) {
    return AlbumGet.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}