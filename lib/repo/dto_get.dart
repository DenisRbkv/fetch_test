class AlbumGet {
  final String text;
  final int number;
  final bool found;
  final String type;


  AlbumGet({this.text, this.number, this.found, this.type});

  factory AlbumGet.fromJson(Map<String, dynamic> json) {
    return AlbumGet(
      text: json['text'],
      number: json['number'],
      found: json['found'],
      type: json['type'],
    );
  }
}