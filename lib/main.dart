import 'package:fetch_test/widgets/post_widget.dart';
import 'package:flutter/material.dart';

import 'validating_func.dart';
import 'widgets/get_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //get example link 'http://numbersapi.com/random/trivia?json'
  //post example link 'https://jsonplaceholder.typicode.com/albums'
  @override
  void initState() {
    FetchService('get', 'http://numbersapi.com/random/trivia?json').reqValid();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Fetch Data Example',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Fetch Data Example'),
        ),
        body: Center(
          child: Container(
            height: 400,
            width: 200,
            child: val ? FuturePage() : PostWidget(), //нет проверки на null
          ),
        ),
      ),
    );
  }
}
