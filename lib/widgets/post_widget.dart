import 'package:flutter/material.dart';
import 'dart:async';

import 'package:fetch_test/repo/dto_post.dart';
import 'package:fetch_test/functions/post.dart';

class PostWidget extends StatefulWidget {
  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {
  final TextEditingController _controller = TextEditingController();
  Future<Album> _futureAlbum;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(8.0),
      child: (_futureAlbum == null)
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'POST',
                  style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 30),
                TextField(
                  controller: _controller,
                  decoration: InputDecoration(hintText: 'Enter Title'),
                ),
                RaisedButton(
                  child: Text('Create Data'),
                  onPressed: () {
                    setState(() {
                      _futureAlbum = createAlbum(_controller.text);
                    });
                  },
                ),
              ],
            )
          : FutureBuilder<Album>(
              future: _futureAlbum,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Text(snapshot.data.title);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }

                return CircularProgressIndicator();
              },
            ),
    );
  }
}
