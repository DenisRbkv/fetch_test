import 'package:flutter/material.dart';
import '../repo/dto_get.dart';

import '../functions/get.dart';

class FuturePage extends StatefulWidget {

  @override
  _FuturePageState createState() => _FuturePageState();
}

class _FuturePageState extends State<FuturePage> {
  Future<AlbumGet> futureAlbum;

  @override
  void initState() {
    super.initState();
    futureAlbum = fetchAlbum();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          'GET',
          style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 30),
        FutureBuilder<AlbumGet>(
          future: futureAlbum,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data.found == true) {
              return Text(snapshot.data.text);
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return CircularProgressIndicator();
          },
        ),
      ],
    );
  }
}
